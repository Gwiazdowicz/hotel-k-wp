<?php
  declare(strict_types=1);
  
  get_header(); ?>

<?php 

get_template_part('parts/hero');
get_template_part('parts/partners'); 
get_template_part('parts/offert'); 
get_template_part('parts/action'); 
get_template_part('parts/projects'); 
get_template_part('parts/clients'); 
get_template_part('parts/about'); 
get_template_part('parts/footer');

?>

<?php get_footer(); ?>
