<?php
  declare(strict_types=1);

  function t4d_theme_settings() {
    add_theme_support('post-thumbnails');
    add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
  }
  add_action('after_setup_theme', 't4d_theme_settings');
  // @Codex:
  // If attached to a hook, it must be ‘after_setup_theme’. The ‘init’ hook may be too late for some features.


  // Register default menus
  function t4d_menus() {
    register_nav_menus(
      array(
        'header-menu' => __('Header Menu'),
        'footer-menu' => __('Footer Menu')
      )
    );
  }
  add_action('init', 't4d_menus');


  // Potential security threat;
  function t4d_remove_login_errors() {
    return 'Something is not right.';
  }
  if (!is_localhost()) {
    add_filter('login_errors', 't4d_remove_login_errors');
  }


  // Remove /users from wp-api
  // Potential security threat;
  function t4d_remove_users_endpoint($endpoints) {
    if (isset($endpoints['/wp/v2/users'])) {
      unset($endpoints['/wp/v2/users']);
    }
    if (isset($endpoints['/wp/v2/users/(?P<id>[\d]+)'])) {
      unset($endpoints['/wp/v2/users/(?P<id>[\d]+)']);
    }
    return $endpoints;
  }
  add_filter('rest_endpoints', 't4d_remove_users_endpoint');


  // Allow only logged in users to use wp_rest api
  function t4d_require_log_in_for_rest_api($result) {
    if (!empty($result)) {
      return $result;
    }
    if (!is_user_logged_in()) {
      return new WP_Error('rest_not_logged_in', 'You are not currently logged in.', array('status' => 401));
    }
    return $result;
  }
  add_filter('rest_authentication_errors', 't4d_require_log_in_for_rest_api');
