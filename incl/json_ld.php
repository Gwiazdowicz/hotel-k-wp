<?php
  declare(strict_types=1);

  function t4d_remove_yoast_json($data) {
    $data = array();
    return $data;
  }
  add_filter('wpseo_json_ld_output', 't4d_remove_yoast_json', 10, 1);

  function t4d_output_structured_data() {
    $logo_url = IMAGES . 'fav/favicon-96x96.png';

    $output = array();

    $base = array();
    $base['@context'] = 'http://schema.org';
    $base['@type'] = '';
    $base['name'] = '';
    $base['alternateName'] = '';
    $base['legalName'] = '';
    $base['url'] = get_home_url() . '/';
    $base['logo'] = $logo_url;

    $base['address']['@type'] = 'PostalAddress';
    $base['address']['addressLocality'] = '';
    $base['address']['streetAddress'] = '';
    $base['address']['postalCode'] = '';
    $base['address']['addressCountry'] = '';

    $base['contactPoint']['@type'] = 'ContactPoint';
    $base['contactPoint']['telephone'] = '';
    $base['contactPoint']['contactType'] = 'Customer service';
    $base['contactPoint']['email'] = '';
    $base['contactPoint']['availableLanguage'] = '';


    $output[] = $base;

    echo '<script type="application/ld+json">';
    echo wp_json_encode($output);
    echo '</script>';
  }
  add_action('wp_head', 't4d_output_structured_data', 10);
