<?php
  declare(strict_types=1);
  
  define("SITE_URL", get_home_url());
  define('TEMPLATE_URL', get_template_directory_uri() . '/');

  define('IMAGES', get_template_directory_uri() . '/assets/img/');
  define('FONTS', get_template_directory_uri() . '/assets/fonts/');
  define('VIDEO', get_template_directory_uri() . '/assets/video/');
  define('CSS', get_template_directory_uri() . '/assets/css/');
  define('JS', get_template_directory_uri() . '/assets/js/');
  define('VENDOR', get_template_directory_uri() . '/assets/vendor/');

  define("ASSETS_V", '0.0.1');

