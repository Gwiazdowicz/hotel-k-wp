<?php $title = get_field('action-title',16);?>
<?php $toptitle = get_field('action-top-title',16);?>
<?php $dispcription = get_field('dispcription',16);?>

    
    <section class="action">
        <div class="action__block">
            <div class="action__border borders">
                <div class="action__container container">
                    <div class="action__box-one">
                        <p class="action__text a-text a-text--small"><?php echo $toptitle;?></p>
                        <h2 class="action__title a-title a-title--small"><?php echo $title;?></h2>
                        <p class="action__discription a-article a-article--white"><?php echo $dispcription ?> </p>
                    </div>
                    <div class="action__box-two">
                        
            
                        <ol id="list-column" class="action__tile">
                            <?php $tiles = get_field('tiles',16);?>

                                <?php while( have_rows('tiles',16)): the_row(); 

                                        $number = get_sub_field('num');
                                        $toptitle = get_sub_field('toptitle');
                                        $title = get_sub_field('title');
                                        $text = get_sub_field('text');

                                ?>
                                <li>
                                    <div class="action__tile-top">
                                        <p class="action__tile-num a-num"><?php echo $number;?></p>
                                        <p class="action__tile-title a-tile-title"><?php echo $title;?></p>                     
                                    </div>
                                    <p class="action__tile-subtitle"><?php echo $text;?></p>
                                    <?php endwhile; ?>
                              </li>   
                        </ol>
                    </div>
                </div>
            </div>  
        </div>  
    </section>


    <style>

    </style>

        