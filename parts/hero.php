<?php
$hero = get_field('title', 9);
$heroNum = get_field('number', 9);
$heroBtn = get_field('button', 9);
$heroLabel = get_field('label', 9);
$heroTitle = get_field('title', 9);
$heroText = get_field('text', 9);
$heroImage = get_field('image', 9);

if( $hero ): ?>
<section class="hero">
    <img class="hero__background" src="https://cdn.pixabay.com/photo/2020/01/22/13/43/mountains-4785337_960_720.jpg";  alt="Smiley face" height="42" width="42">
        <div class="hero__content">
            <div class="hero__border borders">
                <div class="hero__container container">
                    <div class="hero__top-box">
                        <div class="hero__contact-box">
                            <p class="hero__contact-tel"><?php echo esc_attr( $heroNum); ?></p >
                            <a href="!#" class="hero__contact-btn">
                                <p  class="hero__contact-btn-text"><?php echo esc_attr( $heroBtn); ?></p>
                                <div  class="hero__contact-btn-arrow"></div>
                            </a>
                        </div>
                        <div class="hero__logo"></div>
                    </div>
                    <div class="hero__bottom-box">
                        <div class="hero__text-box">
                            <p class="hero__text a-text"><?php echo esc_attr( $heroLabel ); ?></p>
                            <h1 class="hero__title a-title"><?php echo esc_attr( $heroTitle ); ?></h1>
                            <p class="hero__article"><?php echo esc_attr( $heroText  ); ?></p>
                            <div class="hero__play"></div>
                        </div>
                        <div class="hero__form-box"> 
                            <?php echo do_shortcode('[ninja_form id=2]');?></div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
</section>

<?php endif; 
?>