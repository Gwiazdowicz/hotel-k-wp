<?php 
$imageSlider = get_field('slider',43);
?>

<?php 
if( $imageSlider ): ?>
<section class="partners">
    <div class="partners__border border">
        <div class="partners__container">
           <div id="slideshow2">
                <div class="slick slider-2">
                <?php foreach( $imageSlider as $image ): ?>
                        <div  class="box"><div class="partners__icon"></div></div>
                        <div  class="box"><div class="partners__icon"><img  src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div></div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>    
<?php endif; ?> 