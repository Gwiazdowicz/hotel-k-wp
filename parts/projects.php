<?php $slider = get_field('projectslider', 18);?>
<?php $topTitle = get_field('project-toptitle', 18);?>
<?php $title = get_field('titleProject', 18);?>

<section class="projetcts">
    <div class="projetcts__border border">
        <div class="projetcts__container">
        <div class="projetcts__box-top">
            <p class="projetcts__text  a-text a-text--blue a-text--small"><?php echo esc_attr( $topTitle ); ?></p>
            <h2 class="projetcts__title  a-title a-title--blue a-title--small"><?php echo esc_attr($title); ?></h2>
        </div>


        <div id="slideproject">
            <div class="slick slider-3">

                <?php while( have_rows('projectslider',18)): the_row(); 
                                $image = get_sub_field('img');
                                $name = get_sub_field('name');
                                $subtitle = get_sub_field('subtitle');
                                $page = get_sub_field('email');
                                $rwd = get_sub_field('rwd');
                                $rwdImg = get_sub_field('rwd-img');
                            ?>                
                       
                <div class="projetcts__tile">
                    <div class="projetcts__tile_img"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
                    <div class="projetcts__info">
                        <h3 class="projetcts__tile-title"><?php echo esc_attr(  $name ); ?></h3>
                        <p class="projetcts__tile-discription a-article  a-article--realizaction"><?php echo esc_attr( $subtitle ); ?></p>
                        <a href="#!" class="projetcts__tile-webside"><?php echo esc_attr(  $page ); ?></a>
                        <div class="projetcts__tile-rwd">
                            <div class="projetcts__tile-rwd-img"><img src="<?php echo $rwdImg['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
                            <p class="projetcts__tile-rwd-text a-article"><?php echo esc_attr( $rwd); ?></p>
                        </div>
                    </div>
                </div>
                
                <?php endwhile; ?> 

            </div>
        </div>
        </div>
    </div>
</section>

