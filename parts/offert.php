<?php 
$offerttext = get_field('textstart',13);
$textEnd = get_field('textend',13);
$Btn = get_field('btn',13);
?>


<section class="offert">
        <div class="offert__border borders">
            <div class="offert__container container">
                <p class="offert__box-one a-article"> <?php echo esc_attr($offerttext); ?></p>
                <p class="offert__box-two a-article"><?php echo esc_attr($textEnd); ?></p>
                <div class="offert__btn a-btn">
                    <div  class="offert__btn-text "><?php echo esc_attr($Btn); ?></div>
                    <div  class="offert__arrow"></div>
                </div>
            </div>
        </div>    
</section>


