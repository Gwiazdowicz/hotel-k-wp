<?php $clientsTitle = get_field('titleClient', 20);?>
<?php $clientsSlider = get_field('clientslider', 20);?>



<?php 
if( $clientsSlider ): ?>
<section class="clients">
    <div class="clients__border border">
        <div class="clients__container">
            <h2 class="clients__title a-title a-title--small"><?php echo esc_attr($clientsTitle); ?></h2>
                 <div id="slideshow">
                    <div class="slick">
                            <?php while( have_rows('clientslider',20)): the_row(); 
                                $image= get_sub_field('img');
                                $about = get_sub_field('about');
                                $name = get_sub_field('name');
                                $city= get_sub_field('city');
                                ?>  
                                 <div class="clients__tile">
                                    <blockquote class="blockquote-box"> 
                                            <div>
                                                <p class="clients__text a-article  a-article--white"><?php echo $about;?></p>
                                                <footer class="clients__namebig"><?php echo $name;?></footer><br>
                                                <cite class="clients__citybig"><?php echo $city;?></cite>
                                            </div>
                                            
                                            <div class="clients__box-person">
                                                <div class="clients__box-img"><img id="client-img" class="clients__box-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></div>
                                                <div>
                                                    <footer class="clients__name"><?php echo $name;?></footer><br>
                                                    <cite class="clients__city"><?php echo $city;?></cite>
                                                </div>
                                            </div>
                                    </blockquote>   
                                </div>
                        <?php endwhile; ?> 
                
                    </div>
                </div>
        </div>
    </div>
</section>

<?php endif; ?>


