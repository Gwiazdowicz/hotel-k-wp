<?php
$title = get_field('about-title', 22);
$toptitle = get_field('about-toptitle', 22);
$discriptionOne = get_field('about-discriptionone', 22);
$discriptionTwo = get_field('about-discriptiontwo', 22);
$btn = get_field('about-btn', 22);
?>


<section class="about">
        <div class="offert__border borders">
            <div class="offert__container container">
                <p class="action__text a-text a-text--blue"><?php echo $toptitle;?></p>
                <h2 class="about__title a-title a-title--blue a-title--small"><?php echo $title;?></h2>
                <p class="offert__box-one a-article"><?php echo $discriptionOne;?></p>
                <p class="about__box-two a-article"><?php echo $discriptionTwo;?></p>
                <div class="offert__btn a-btn">
                    <div  class="offert__btn-text"><?php echo $btn;?></div>
                    <div  class="offert__arrow"></div>
                </div>
            </div>
        </div>    
</section>














