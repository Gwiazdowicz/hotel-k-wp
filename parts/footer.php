<?php 
$footerTitle = get_field('titlef',24);
$footerText = get_field('subtitle',24);
?>

<section class="footer">
    <div class="footer__border border">
        <div class="footer__container">
            <div class="footer__box-top">
                <h2 class="footer__title a-title a-title--small"><?php echo esc_attr($footerTitle); ?></h2>
                <p class="footer__text a-article a-article--white"> <?php echo esc_attr($footerText); ?></p>
            </div>
            <div class="footer__box-form hero__form-box">
                <?php echo do_shortcode('[ninja_form id=2]');?></div>
            </div>
            <div class="footer__box-icons">
                <a href ="#!" class="footer__icon-insta  "></a>
                <a href ="#!" class="footer__icon-fb "></a>
                <a href ="#!" class="footer__icon-tweet "></a>
            </div>
            <div class="footer__box-icons-big">
                <div class="footer__box-icon-text">
                    <a href ="#!" class="footer__icon-insta"></a>
                    <p class="footer__lasttext">Instagram</p>
                </div>
                <div class="footer__box-icon-text">
                    <a href ="#!" class="footer__icon-fb "></a>
                    <p class="footer__lasttext">Facebook</p>
                </div>
                <div class="footer__box-icon-text">
                    <a href ="#!" class="footer__icon-tweet "></a>
                    <p class="footer__lasttext">Tweeter</p>
                </div>
            </div>
        </div>
    </div>
</section>
</main>
</body>
</html>
