<?php
  declare(strict_types=1);  
?>
<!DOCTYPE html>
<html class="no-js" lang="<?= get_bloginfo('language'); ?>">
<head>
    <meta charset="utf-8">  
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php wp_title(); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Check if JS is enabled, remove html "no-js" class    -->
    <script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
    
    <link href="https://fonts.googleapis.com/css?family=Barlow|Montserrat:400,600,700,700i,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Barlow:400,500,700,800&display=swap" rel="stylesheet">
    
	<?php wp_head(); ?>
    
</head>
<body <?= body_class(); ?>>
<!--[if lt IE 11]>
    <p>Używasz bardzo starej wersji przeglądarki Internet Explorer. Z&nbsp;tego też powodu ta&nbsp;strona najprawdopodobniej nie&nbsp;działa prawidłowo oraz narażasz się na potencjalne ryzyko związane z bezpieczeństwem.
    <a href="https://www.microsoft.com/pl-pl/download/Internet-Explorer-11-for-Windows-7-details.aspx" rel="noopener noreferrer">Wejdź tutaj, aby ją zaktualizować.</a></p>
<![endif]-->
