# Clean WP Theme Boilerplate

###TL;DR
* **npm run dev** - no minification, sets up dev server proxy with live reload at localhost:3000
* **npm run build** - optimized for production 

##

####Feats:
* Head cleanup - removes all the unnecessary mess WordPress makes
* Basic JSON-LD generation
* Compression
* Async/defer script enqueues

####Build process:
All assets are processed by Webpack. <br>
Basic config is available in
> _src/compiler/config.js<br>

