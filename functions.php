<?php
  declare(strict_types=1);
  
  define('TEMPLATE_DIR', get_template_directory() . '/');

  require_once(TEMPLATE_DIR . 'incl/helper_functions.php');
  require_once(TEMPLATE_DIR . 'incl/constants.php');
  require_once(TEMPLATE_DIR . 'incl/wp_cleanup.php');
  require_once(TEMPLATE_DIR . 'incl/site_setup.php');
  require_once(TEMPLATE_DIR . 'incl/json_ld.php');
  require_once(TEMPLATE_DIR . 'incl/enqueue_scripts.php');
  require_once(TEMPLATE_DIR . 'incl/enqueue_styles.php');
  require_once(TEMPLATE_DIR . 'incl/performance.php');


  if (!is_localhost()) {
    /*  
     * Compresson settings to be found inside.
     * CSS - false, JS - false - they are handled by build process.   
    */
    require_once(TEMPLATE_DIR . 'incl/compression.php');
  }
